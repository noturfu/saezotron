# coding: utf-8
"""
# TODO
"""
import os
import sys
from time import sleep

from random import choice, randrange


from tweepy import StreamListener, Stream, OAuthHandler, API

# OAuth access tokens
from secrets import consumer_key, consumer_secret
from secrets import access_token, access_token_secret

# max numbers of tweets
TWEETS = 2
# every number of hours
MIN_TIME = 12

class Saezotron:
    words = {
        "insult": [
            "putain", "enculay"
        ],
        "beaufs": [
            "le peuple de france",
            "la jeunesse moutonisay",
            "les enculay", "peuple putassiay"
        ],
        "poeple_he_doesnt_like": ["les ptites putes", "les prostituay",
                                  "les beaufs français",
                                  "le peuple anesthésiay"],
        "them": ["les financiay",
                 "la télé réalitay", "les députay",
                 "la societay", "les banquiers",
                 "le nouvel ordre mondialisay"],
        "bad_things": ["du blé", "du pouvoir centralisay", "des pepettes", "la telay"],
        "social_medias": ["twitter", "instagram", "facebook"]
    }
    actions = {
            "positive": [
                "se revoltay", "se levay",
                "lancer des pavay", "se reveillay",
                "refaire le mois de may"
            ],
            "negative": [
                "se faire niquay",
                "se faire sodomisay", "être zombifiay",
                "se faire lobotomisay", "se faire enculay",
                "se faire crevay"
            ],
        }
    misogyny = {
        "people": ["les ptites putes",
                   "les prostituay",
                   "les groopies hystérisay",
                   "les manequins glacay",
                   "les adolescente"],
        "slut_shaming": ["mal baisay", "hystérisay", "décharnay", "photoshopay", "camay", "frigidifiay"]
    }


    def get_word(self, key):
        return self.words.get(key)

    def get_words(self, keys):
        for key in keys:
            yield self.words.get(key)

    def get_rand_word(self, key):
        return choice(self.words.get(key))

    def get_words(self, *keys):
        already_said = []
        chosen = None
        for group, key in keys:
            words = group.get(key)
            if words is not None:
                while chosen in already_said or chosen is None:
                    chosen = choice(words)
                already_said.append(chosen)
                yield chosen

    def rant(self):
        rants = [
            self.on_va,
            self.slut_shame,
        ]
        rant = choice(rants)
        return rant()


    def on_va(self):
        return """
Extrait de «On va {}» #Humanitay

"Dans cette {} de societay

{} par {}
{}
Par {}, {}
C'est le reigne {}

Allez {} faut {}
Ou on va {}"
        """.format(*self.get_words(
            (self.actions, "negative"),                                         # title

            (self.words, "insult"),

            (self.actions, "negative"), (self.words, "them"),
            (self.actions, "negative"),
            (self.words, "them"), (self.words, "them"),
            (self.words, "bad_things"),

            (self.words, "beaufs"), (self.actions, "positive"),
            (self.actions, "negative")
        ))

    def slut_shame(self):
        return """
Extrait de «{}» #Humanitay

"{} sur {}

{}, {}, {}
{} pour {}

Collabo avec {}"
        """.format(*self.get_words(
            (self.misogyny, "people"),
            (self.misogyny, "people"), (self.words, "social_medias"),
            (self.misogyny, "slut_shaming"), (self.misogyny, "slut_shaming"), (self.misogyny, "slut_shaming"),
            (self.misogyny, "slut_shaming"), (self.misogyny, "bad_things"),
            (self.words, "them"),
            (self.words, "them"),
        ))


def rant_print(obj):
    message = obj.rant()
    print("Generated:\n {}".format(message))
    print("(len: {})".format(len(message)))
    print("\n-----------------------------------------------------\n")
    return message


if __name__ == '__main__':
    print("Initializing Saezotron")
    saez = Saezotron()

    if consumer_key == 'EDIT ME' and '--tweet' in sys.argv:
        raise ImportError("Please configure OAuth in secret folder")

    if '--tweet' in sys.argv:
        print("Authenticating to tweeter with OAuth")

        # auth
        auth = OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)

        # API
        api = API(auth)

    while True:
        if '--now' in sys.argv:
            message = rant_print(saez)
            if '--tweet' in sys.argv:
                twitter.update_status(status=message)

        # Setting next tweets time
        hours = range(MIN_TIME)
        minutes = range(60)
        tweets_hours = [(choice(hours), choice(minutes))
                        for tweet in range(TWEETS)]

        for tweet in tweets_hours:
            print(
                'Scheduled a tweet in {} hours and {} minutes'.format(*tweet)
            )
        sleep_times = [3600*hours + minutes*60
                       for hours, minutes in tweets_hours]

        for sleep_time in sleep_times:
            print('Sleeping for {} minutes ({} hours)'.format(sleep_time / 60, sleep_time / 3600))
            if not '--skip_sleep' in sys.argv:
                sleep(sleep_time)
            if '--tweet' in sys.argv:
                print("Tweeting...")
                message = rant_print(saez)
                twitter.update_status(status=message)


